<?php

namespace App\Http\Controllers;

use App\Models\Voluntario;
use Illuminate\Http\Request;

class VoluntarioController extends Controller
{

    public function index()
    {
        $voluntarios = Voluntario::all();

        return view('voluntarios.index', compact('voluntarios'));
    }

    public function create()
    {
        return view('voluntarios.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'nome' => 'required',
            'cpf' => 'required',
            'email' => 'required',
        ]);

        Voluntario::create($request->all());

        return redirect()->route('voluntarios.create')->with('mensagem', 'Voluntario cadastrado com SUCESSO!!');
    }


    public function show(Voluntario $voluntario)
    {
        return view('voluntarios.show', compact('voluntario'));
    }


    public function edit(Voluntario $voluntario)
    {
        return view('voluntarios.edit', compact('voluntario'));
    }


    public function update(Request $request, Voluntario $voluntario)
    {
        $request->validate([
            'nome' => 'required',
            'email' => 'required',
            'cpf' => 'required',
        ]);
        
        $voluntario->update($request->all());

        return redirect()->route('voluntarios.index')->with('mensagem', 'Voluntario atualizado com Sucesso');
    }

    public function destroy(Voluntario $voluntario)
    {
        $voluntario->delete();

        return redirect()->route('voluntarios.index')->with('mensagem', 'Voluntario Excluido com Sucesso');
    }
}
