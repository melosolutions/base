<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>voluntarios</title>
</head>

<body>
    <h1>voluntarios Cadastrados</h1>

@if($status = Session::get('mensagem'))
  <h2>{{ $status }}</h2>
@endif

<h4><a href="{{ route('voluntarios.create') }}">Cadastrar 
  Novo Livro</a></h4>

<table width="709" border="1" cellspacing="0" cellpadding="3">
  <tr>
    <td width="85" align="center"><strong>Id</strong></td>
    <td width="161" align="center"><strong>Nome</strong></td>
    <td width="156" align="center"><strong>E-mail</strong></td>
    <td width="98" align="center"><strong>CPF</strong></td>
    <td width="167" align="center"><strong>Opções</strong></td>
  </tr>
  
  @foreach($voluntarios as $voluntario)
  
  <tr>
      <td align="center">{{ $voluntario->id }}</td>
      <td>{{ $voluntario->nome }}</td>
      <td>{{ $voluntario->email }}</td>
      <td align="center">{{ $voluntario->cpf }}</td>
      <td align="center">
      
      <form action="{{ route('voluntarios.destroy', $voluntario->id) }}" 
         method="post">
         <a href="{{ route('voluntarios.show', 
           $voluntario->id) }}">Detalhes</a> | 
           <a href="{{ route('voluntarios.edit', 
           $voluntario->id) }}">Editar</a>
      
         @csrf
         @method('DELETE')
      
         <button type="submit">Excluir</button>
      </form>
      
    </tr>
  @endforeach
  
</table>


</body>
</html>