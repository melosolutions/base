<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    
    <h1>Detalhes do Voluntario</h1>

    <table width="500" border="1" cellspacing="0" cellpadding="3">
        <tr>
          <td><strong>Id</strong></td>
          <td>{{ $voluntario->id }}</td>
        </tr>
        <tr>
          <td><strong>Nome</strong></td>
          <td> {{ $voluntario->nome }}</td>
        </tr>
        <tr>
          <td><strong>E-mail</strong></td>
          <td>{{ $voluntario->email }}</td>
        </tr>
        <tr>
          <td><strong>CPF</strong></td>
          <td>{{ $voluntario->cpf }}</td>
        </tr>
      </table>

</body>
</html>