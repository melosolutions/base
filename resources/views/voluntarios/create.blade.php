<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cadastro</title>
</head>
<body>
    <h1>Novo Livro</h1>
 
@if($status = Session::get('mensagem'))
  <h2>{{ $status }}</h2>
@endif
 
@if($errors->any())
  <h2>Houve alguns erros ao processar o formulário</h2>
  <ul>
     @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
     @endforeach
  </ul>
@endif
 
<form action="{{ route('voluntarios.store') }}" method="post">
@csrf
<table width="200" border="0" cellspacing="3" 
  cellpadding="3">
  <tr>
    <td>Nome:</td>
    <td><input type="text" name="nome" id="nome" 
      placeholder="Nome"></td>
  </tr>
  <tr>
    <td>Email:</td>
    <td><input type="text" name="email" id="email"
      placeholder="E-mail"></td>
  </tr>
  <tr>
    <td>CPF:</td>
    <td><input type="text" size="10" name="cpf" 
      id="cpf" placeholder="CPF"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><button type="submit">Cadastrar</button></td>
  </tr>
</table>
</form>

</body>
</html>