<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>

    <h1>Atualizar Voluntario</h1>

@if($errors->any())
  <h2>Houve alguns erros ao processar o formulário</h2>
  <ul>
     @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
     @endforeach
  </ul>
@endif

<form action="{{ route('voluntarios.update', $voluntario->id) }}" method="post">
@csrf
@method('PUT')

<table width="200" border="0" cellspacing="3" cellpadding="3">
  <tr>
    <td>Título:</td>
    <td><input type="text" value="{{ $voluntario->nome }}" 
      name="nome" id="nome" placeholder="nome"></td>
  </tr>
  <tr>
    <td>Autor:</td>
    <td><input type="text" value="{{ $voluntario->email }}" 
      name="email" id="email" placeholder="email"></td>
  </tr>
  <tr>
    <td>Páginas</td>
    <td><input type="text" value="{{ $voluntario->cpf }}" 
      size="10" name="cpf" id="cpf" 
      placeholder="cpf"></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><button type="submit">Salvar Alterações</button></td>
  </tr>
</table>

</body>
</html>